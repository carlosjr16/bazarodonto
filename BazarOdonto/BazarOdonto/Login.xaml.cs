﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BazarOdonto
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CadastroUsuLogin : ContentPage
	{
		public CadastroUsuLogin ()
		{
			InitializeComponent ();
		}
        private async void Btn_CadUsu(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new CadastroUsu());
        }
    }
}