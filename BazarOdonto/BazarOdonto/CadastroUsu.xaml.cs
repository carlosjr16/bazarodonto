﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BazarOdonto
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CadastroUsu : ContentPage
    {
        public CadastroUsu()
        {
            InitializeComponent();
        }
        private async void Btn_Termos(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Termos());
        }
    }
}